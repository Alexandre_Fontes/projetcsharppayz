﻿/* Author: Mauro Santos
 * Date: 05.12.2019
 * Description: Manages every connection to the sql database
 */
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
//code adapted from https://www.codeproject.com/Articles/43438/Connect-C-to-MySQL
namespace Model
{
    public class DbConnector
    {
        private MySqlConnection connection;
        private string server;
        private int port;
        private string database;
        private string uid;
        private string password;
        /// <summary>
        /// Constructor that initializes the connection
        /// </summary>
        public DbConnector()
        {
            Initialize();
        }
        /// <summary>
        /// Function that Initializes the connection with the data base
        /// </summary>
        private void Initialize()
        {
            server = "payzdb.mysql.database.azure.com";
            port = 3306;
            database = "payz";
            uid = "userpayz@payzdb";
            password = "Pa$$w0rd";
            string connectionString;
            connectionString = "Server=" + server + ";Port=" + port + ";Database=" + database + ";Uid=" + uid + ";Pwd=" + password + ";";
            connection = new MySqlConnection(connectionString);
        }

        /// <summary>
        /// Function that opens connection to the sqlServer
        /// </summary>
        /// <returns>true if connection was successful and false if it wasn't</returns>
        private bool OpenConnection()
        {
            try
            {
                connection.Open();
                return true;
            }
            catch (MySqlException ex)
            {
                //When handling errors, you can your application's response based 
                //on the error number.
                //The two most common error numbers when connecting are as follows:
                //0: Cannot connect to server.
                //1045: Invalid user name and/or password.
                switch (ex.Number)
                {
                    case 0:
                        MessageBox.Show("Cannot connect to server.  Contact administrator");
                        break;

                    case 1045:
                        MessageBox.Show("Invalid username/password, please try again");
                        break;
                }
                return false;
            }
        }

        /// <summary>
        /// Function that Closes connection to the sqlServer
        /// </summary>
        /// <returns></returns>
        private bool CloseConnection()
        {
            try
            {
                connection.Close();
                return true;
            }
            catch (MySqlException ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }
        /// <summary>
        /// Function that executes an Insert query
        /// </summary>
        /// <param name="query">Query to execute</param>
        public void Insert(string query)
        {

            //open connection
            if (this.OpenConnection() == true)
            {
                //create command and assign the query and connection from the constructor
                MySqlCommand cmd = new MySqlCommand(query, connection);

                //Execute command
                cmd.ExecuteNonQuery();

                //close connection
                this.CloseConnection();
            }
            else
            {
                throw new SqlConnectionError();
            }
        }
        /// <summary>
        /// Function that executes a Select query and returns the value
        /// </summary>
        /// <param name="query">>Query to execute</param>
        /// <returns>array of string lists with the results from the query</returns>
        public List<string>[] SelectUser(string query)
        {


            //Create a list to store the result
            List<string>[] list = new List<string>[3];
            list[0] = new List<string>();
            list[1] = new List<string>();
            list[2] = new List<string>();

            //Open connection
            if (this.OpenConnection() == true)
            {
                //Create Command
                MySqlCommand cmd = new MySqlCommand(query, connection);
                //Create a data reader and Execute the command
                MySqlDataReader dataReader = cmd.ExecuteReader();

                //Read the data and store them in the list
                while (dataReader.Read())
                {
                    list[0].Add(dataReader["Email"] + "");
                    list[1].Add(dataReader["Password"] + "");
                    list[2].Add(dataReader["IdUser"] + "");
                }

                //close Data Reader
                dataReader.Close();

                //close Connection
                this.CloseConnection();

                //return list to be displayed
                return list;
            }
            else
            {
                throw new SqlConnectionError();
            }
        }
        public List<string>[] SelectPayement(string query)
        {
            //Create a list to store the result
            List<string>[] list = new List<string>[13];
            for (int i = 0; i < 13; i++)
            {
                list[i] = new List<string>();
            }
            //Open connection
            if (this.OpenConnection() == true)
            {
                //Create Command
                MySqlCommand cmd = new MySqlCommand(query, connection);
                //Create a data reader and Execute the command
                MySqlDataReader dataReader = cmd.ExecuteReader();

                //Read the data and store them in the list
                while (dataReader.Read())
                {
                    for (int i = 0; i < 13; i++)
                    {
                        list[i].Add(dataReader[i] + "");
                    }
                }

                //close Data Reader
                dataReader.Close();

                //close Connection
                this.CloseConnection();

                //return list to be displayed
                return list;
            }
            else
            {
                throw new SqlConnectionError();
            }
        }

    }
    /// <summary>
    /// Exception that is thrown when the sql server isn't available
    /// </summary>
    public class SqlConnectionError : Exception
    {

    }
}
