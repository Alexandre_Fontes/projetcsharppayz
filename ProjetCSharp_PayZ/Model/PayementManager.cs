﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public static class PaymentManager
    {
        /// <summary>
        /// untested 
        /// </summary>
        /// <returns></returns>
        public static List<Payment> GetUserPayments(bool hidePayed, bool sortID)
        {
            List<Payment> payements = new List<Payment>();
            DbConnector connector = new DbConnector();
            string query = "SELECT * FROM payements WHERE FKUser =  '" + Session.ID+"'";
            if (hidePayed)
            {
                query = query + "AND STATUS='0'";
            }
            if (sortID)
            {
                query = query + "Order by IDPayement"; 
            }
            else
            {
                query = query + "Order by PayementDate";
            }
            query = query+ ";";
            List<string>[] result = connector.SelectPayement(query);

            for(int i = 0;i<result[0].Count;i++)
            {
                payements.Add(new Payment(int.Parse(result[0][i]), int.Parse(result[3][i]), int.Parse(result[7][i]), int.Parse(result[11][i]), float.Parse(result[2][i]), result[4][i], result[5][i], result[8][i], result[9][i], result[10][i],result[12][i], DateTime.Parse(result[6][i])));
            }

            return payements;
        }
        /// <summary>
        /// untested 
        /// </summary>
        /// <param name="payement"></param>
        public static void SavePayment(Payment payement)
        {
            DbConnector connector = new DbConnector();
            string query = "INSERT INTO payements (FKUser,Amount,Status,PayementName,Description,PayementDate,MaxDelay,RecipientName,RecipientEmail,City,PostalCode,Address)" +"VALUES("+ Session.ID +", "+payement.Amount +", "+payement.Status+", \""+payement.PaymentName+"\", \""+payement.Description+"\", \""+payement.PayementDate.ToString("yyyy/MM/dd")+"\", "+payement.MaxDelay+", \""+payement.RecipientName+ "\", \"" + payement.RecipientEmail + "\", \"" + payement.City + "\", "+payement.PostalCode+ ", \"" + payement.Address + "\")";
            connector.Insert(query);
        }
        /// <summary>
        /// untested 
        /// </summary>
        /// <param name="payement"></param>
        public static void DeletePayment(Payment payement)
        {
            DbConnector connector = new DbConnector();
            string query = "DELETE FROM payements WHERE IDPayement="+payement.IdPayment+";";
            connector.Insert(query);
        }
        public static void UpdatePayment(Payment payement)
        {
            DbConnector connector = new DbConnector();
            string query = "Update payements SET Amount="+payement.Amount+ ", PayementName=\"" + payement.PaymentName + "\", Description=\"" + payement.Description + "\", PayementDate=\"" + payement.PayementDate.ToString("yyyy/MM/dd") + "\", MaxDelay=" + payement.MaxDelay + ", RecipientName=\"" + payement.RecipientName + "\", RecipientEmail=\"" + payement.RecipientEmail + "\", City=\"" + payement.City + "\", PostalCode=" + payement.PostalCode + " WHERE IDPayement ="+payement.IdPayment+";";
            connector.Insert(query);
        }
        public static void Pay(Payment payement)
        {
            DbConnector connector = new DbConnector();
            string query = "Update payements SET Status = 1 WHERE IDPayement =" + payement.IdPayment + ";";
            connector.Insert(query);
        }
    }
}
    