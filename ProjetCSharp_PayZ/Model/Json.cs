﻿/* Author: Mauro Santos
 * Date: 05.12.2019
 * Description: Handles the reading and writing from a json file
 */
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class Json
    {
        const string path = @"Json\";
        /// <summary>
        /// Function that reads the contents of the user's json file and turns them into userSettings
        /// </summary>
        /// <param name="email">user that we want the settings from</param>
        /// <returns>a userSettings object containing the user's settings</returns>
        public static UserSettings Read(string email)
        {
            UserSettings settingsFromJson = null;
            if (File.Exists(path + email + ".json"))
            {
                string jsonFromFile;
                using (var reader = new StreamReader(path + email + ".json"))
                {
                    jsonFromFile = reader.ReadToEnd();
                }
                settingsFromJson = JsonConvert.DeserializeObject<UserSettings>(jsonFromFile);
            }
            else
            {
                settingsFromJson = new UserSettings(0, 0, 661, 308);
                Console.WriteLine(path + email + ".json");
            }
            return settingsFromJson;
        }
        /// <summary>
        /// Function that writes the current settings on to the user's json file
        /// </summary>
        /// <param name="email">email of the user that we have to change</param>
        /// <param name="settings">settings to save on the file</param>
        public static void Write(string email, UserSettings settings)
        {
            string output = JsonConvert.SerializeObject(settings);
            if (!System.IO.File.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            using (StreamWriter writer = new StreamWriter(path + email + ".json"))
            {
                writer.WriteLine(output);
            }
        }
    }
}
