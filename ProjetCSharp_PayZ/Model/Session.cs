﻿/* Author: Mauro Santos
 * Date: 05.12.2019
 * Description: Saves the current settings the user
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public static class Session
    {
        private static string email;
        private static int id;
        private static UserSettings settings;
        private static List<Payment> payments= new List<Payment>();
        public static string Email
        {
            get
            {
                return email;
            }
            set
            {
                email = value;
            }
        }
        public static int ID
        {
            get
            {
                return id;
            }
            set
            {
                id = value;
            }
        }
        public static List<Payment> Payments
        {
            get
            {
                return payments;
            }
            set
            {
                payments = value;
            }
        }
        public static UserSettings Settings
        {
            get
            {
                return settings;
            }
            set
            {
                settings = value;
            }
        }
        /// <summary>
        /// Reads user settings from his Json File
        /// </summary>
        public static void UpdateSettings()
        {
            settings = Json.Read(email);

        }

        /// <summary>
        /// Saves user settings to his Json File
        /// </summary>
        public static void SaveSettings()
        {
            Json.Write(email, settings);
        }
    }
}
