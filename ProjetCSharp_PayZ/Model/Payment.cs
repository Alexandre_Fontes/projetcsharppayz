﻿/* Author: Mauro Santos
 * Date: 05.12.2019
 * Description: Class that stores payment informations
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class Payment
    {
        private int idPayment, status, maxDelay,postalCode;
        private float amount;
        private string paymentName, description, recipientName , recipientEmail,city,address;
        DateTime payementDate;
        public Payment()
        {

        }
        public Payment(int idPayement, int status, int maxDelay,int postalCode, float amount, string payementName, string description, string recipientName, string recipientEmail,string city,string address,  DateTime payementDate)
        {
            this.idPayment = idPayement;
            this.status = status;
            this.maxDelay = maxDelay;
            this.postalCode = postalCode; 
            this.amount = amount;
            this.paymentName = payementName;
            this.description = description;
            this.recipientName = recipientName;
            this.recipientEmail = recipientEmail;
            this.city = city;
            this.address = address;
            this.payementDate = payementDate;
        }

        public int IdPayment { get => idPayment; set => idPayment = value; }
        public int Status { get => status; set => status = value; }
        public int MaxDelay { get => maxDelay; set => maxDelay = value; }
        public int PostalCode { get => postalCode; set => postalCode = value; }
        public float Amount { get => amount; set => amount = value; }
        public string PaymentName { get => paymentName; set => paymentName = value; }
        public string Description { get => description; set => description = value; }
        public string RecipientName { get => recipientName; set => recipientName = value; }
        public string RecipientEmail { get => recipientEmail; set => recipientEmail = value; }
        public string City { get => city; set => city = value; }
        public string Address { get => address; set => address = value; }
        public DateTime PayementDate { get => payementDate; set => payementDate = value; }
        
    }
}
