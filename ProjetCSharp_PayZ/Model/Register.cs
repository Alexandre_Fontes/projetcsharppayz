﻿/* Author: Mauro Santos
 * Date: 05.12.2019
 * Description: Contaitns all of the functions needed for the RegisterPage
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class Register
    {
        /// <summary>
        /// This function checks if the register was possible, checking if the email is already used or not
        /// </summary>
        /// <param name="email">User's email</param>
        /// <param name="password">User's password</param>
        /// <returns>True if the register was successful or an exception if there's a problem </returns>
        public static bool ValidRegister(string email, string password)
        {
            DbConnector connector = new DbConnector();
            string query = "SELECT Email,Password FROM users WHERE Email = '" + email + "';";
            List<string>[] result = connector.SelectUser(query);
            if (result[1].Count == 0 && result[0].Count == 0)
            {
                string hash = Hasher.GetHashString(password).ToLower();
                connector.Insert("INSERT INTO users (Email,Password)VALUES ('"+email+"','"+ hash + "')");
                return true;
            }
            else
            {
                throw new ExistingEmailException();
            }
        }
    }
    /// <summary>
    /// Exception thrown when there's a user with the same email on the database
    /// </summary>
    public class ExistingEmailException : Exception
    {

    }
}
