﻿/* Author: Mauro Santos
 * Date: 05.12.2019
 * Description: Hashes de password
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
///https://stackoverflow.com/questions/3984138/hash-string-in-c-sharp
namespace Model
{
    public class Hasher
    {
        /// <summary>
        /// Function that takes a string and hashes it using SHA256
        /// </summary>
        /// <param name="inputString">string to hash</param>
        /// <returns>Hashed bits</returns>
        public static byte[] GetHash(string inputString)
        {
            HashAlgorithm algorithm = SHA256.Create();
            return algorithm.ComputeHash(Encoding.UTF8.GetBytes(inputString));
        }
        /// <summary>
        /// Function that takes a string and hashes it using SHA256
        /// </summary>
        /// <param name="inputString">string to hash</param>
        /// <returns>Hashed string</returns>
        public static string GetHashString(string inputString)
        {
            StringBuilder sb = new StringBuilder();
            foreach (byte b in GetHash(inputString))
                sb.Append(b.ToString("X2"));

            return sb.ToString();
        }
    }
}
