﻿/* Author: Mauro Santos
 * Date: 05.12.2019
 * Description: Contaitns all of the functions needed for the LoginPage
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class Login
    {
        /// <summary>
        /// This function checks if the login was possible, checking if the email and password correspond to anyone on the database
        /// </summary>
        /// <param name="email">email that we are trying to login to</param>
        /// <param name="password">user's password</param>
        /// <returns>true if the login passed and throws an exception if there's an error </returns>
        static public bool ValidLogin(string email, string password)
        {
            DbConnector connector = new DbConnector();
            string query = "SELECT Email,Password,IdUser FROM users WHERE Email = '" + email + "';";
            List<string>[] result = connector.SelectUser(query);
            if (result[1].Count == 1 && result[0].Count == 1)
            {
                string hash = Hasher.GetHashString(password).ToLower();
                if (result[1][0] == hash)
                {
                    int.TryParse(result[2][0],out int temp);
                    Session.ID = temp;
                    return true;
                }
                else
                {
                    throw new WrongPasswordException();
                }
            }
            else
            {
                throw new WrongEmailException();
            }
            
        }
        
    }
    /// <summary>
    /// Exception thrown when the email isn't on the dataBase
    /// </summary>
    public class WrongEmailException : Exception
    {

    }
    /// <summary>
    /// Exception thrown when the password is the same as the one on the database
    /// </summary>
    public class WrongPasswordException : Exception
    {

    }
}
