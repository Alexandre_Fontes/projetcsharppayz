﻿/* Author: Mauro Santos
 * Date: 05.12.2019
 * Description: Stores the current window settings
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class UserSettings
    {
        private int positionX, positionY, sizeX, sizeY;
        /// <summary>
        /// constructor without paramaters
        /// </summary>
        public UserSettings()
        {
        }

        /// <summary>
        /// constructor with paramaters
        /// </summary>
        /// <param name="positionX">window's position on the X axis</param>
        /// <param name="positionY">window's position on the Y axis</param>
        /// <param name="sizeX">window's Height</param>
        /// <param name="sizeY">window's Width</param>
        public UserSettings(int positionX, int positionY, int sizeX, int sizeY)
        {
            this.positionX = positionX;
            this.positionY = positionY;
            this.sizeX = sizeX;
            this.sizeY = sizeY;
        }

        public int PositionX
        {
            get
            {
                return positionX;
            }
            set
            {
                positionX = value;
            }
        }
        public int PositionY
        {
            get
            {
                return positionY;
            }
            set
            {
                positionY = value;
            }
        }
        public int SizeX
        {
            get
            {
                return sizeX;
            }
            set
            {
                sizeX = value;
            }
        }
        public int SizeY
        {
            get
            {
                return sizeY;
            }
            set
            {
                sizeY = value;
            }
        }
    }
}
