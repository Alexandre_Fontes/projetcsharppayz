﻿namespace ProjetCSharp_PayZ.View
{
    partial class PaymentPage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.lbl_ID = new System.Windows.Forms.Label();
            this.lbl_Nom = new System.Windows.Forms.Label();
            this.lbl_Prix = new System.Windows.Forms.Label();
            this.lbl_Statut = new System.Windows.Forms.Label();
            this.lbl_DatePaiement = new System.Windows.Forms.Label();
            this.lbl_DateEcheance = new System.Windows.Forms.Label();
            this.btn_Ajouter = new System.Windows.Forms.Button();
            this.btn_Supp = new System.Windows.Forms.Button();
            this.btn_Deco = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lst_Info = new System.Windows.Forms.ListBox();
            this.btn_Modifier = new System.Windows.Forms.Button();
            this.Grp_Filtres = new System.Windows.Forms.GroupBox();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.Chk_HidePayed = new System.Windows.Forms.CheckBox();
            this.groupBox1.SuspendLayout();
            this.Grp_Filtres.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(363, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(254, 32);
            this.label1.TabIndex = 0;
            this.label1.Text = "Listes de paiement";
            // 
            // lbl_ID
            // 
            this.lbl_ID.AutoSize = true;
            this.lbl_ID.Location = new System.Drawing.Point(53, 106);
            this.lbl_ID.Name = "lbl_ID";
            this.lbl_ID.Size = new System.Drawing.Size(24, 17);
            this.lbl_ID.TabIndex = 1;
            this.lbl_ID.Text = "N°";
            // 
            // lbl_Nom
            // 
            this.lbl_Nom.AutoSize = true;
            this.lbl_Nom.Location = new System.Drawing.Point(167, 106);
            this.lbl_Nom.Name = "lbl_Nom";
            this.lbl_Nom.Size = new System.Drawing.Size(37, 17);
            this.lbl_Nom.TabIndex = 2;
            this.lbl_Nom.Text = "Nom";
            // 
            // lbl_Prix
            // 
            this.lbl_Prix.AutoSize = true;
            this.lbl_Prix.Location = new System.Drawing.Point(329, 106);
            this.lbl_Prix.Name = "lbl_Prix";
            this.lbl_Prix.Size = new System.Drawing.Size(31, 17);
            this.lbl_Prix.TabIndex = 3;
            this.lbl_Prix.Text = "Prix";
            // 
            // lbl_Statut
            // 
            this.lbl_Statut.AutoSize = true;
            this.lbl_Statut.Location = new System.Drawing.Point(483, 106);
            this.lbl_Statut.Name = "lbl_Statut";
            this.lbl_Statut.Size = new System.Drawing.Size(45, 17);
            this.lbl_Statut.TabIndex = 4;
            this.lbl_Statut.Text = "Statut";
            // 
            // lbl_DatePaiement
            // 
            this.lbl_DatePaiement.AutoSize = true;
            this.lbl_DatePaiement.Location = new System.Drawing.Point(644, 106);
            this.lbl_DatePaiement.Name = "lbl_DatePaiement";
            this.lbl_DatePaiement.Size = new System.Drawing.Size(120, 17);
            this.lbl_DatePaiement.TabIndex = 5;
            this.lbl_DatePaiement.Text = "Date de paiement";
            // 
            // lbl_DateEcheance
            // 
            this.lbl_DateEcheance.AutoSize = true;
            this.lbl_DateEcheance.Location = new System.Drawing.Point(813, 106);
            this.lbl_DateEcheance.Name = "lbl_DateEcheance";
            this.lbl_DateEcheance.Size = new System.Drawing.Size(115, 17);
            this.lbl_DateEcheance.TabIndex = 6;
            this.lbl_DateEcheance.Text = "Date d\'échéance";
            // 
            // btn_Ajouter
            // 
            this.btn_Ajouter.Location = new System.Drawing.Point(35, 434);
            this.btn_Ajouter.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn_Ajouter.Name = "btn_Ajouter";
            this.btn_Ajouter.Size = new System.Drawing.Size(160, 33);
            this.btn_Ajouter.TabIndex = 1;
            this.btn_Ajouter.Text = "Ajouter une facture";
            this.btn_Ajouter.UseVisualStyleBackColor = true;
            this.btn_Ajouter.Click += new System.EventHandler(this.btn_Ajouter_Click);
            // 
            // btn_Supp
            // 
            this.btn_Supp.Location = new System.Drawing.Point(227, 434);
            this.btn_Supp.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn_Supp.Name = "btn_Supp";
            this.btn_Supp.Size = new System.Drawing.Size(160, 33);
            this.btn_Supp.TabIndex = 2;
            this.btn_Supp.Text = "Supprimer";
            this.btn_Supp.UseVisualStyleBackColor = true;
            this.btn_Supp.Click += new System.EventHandler(this.button3_Click);
            // 
            // btn_Deco
            // 
            this.btn_Deco.Location = new System.Drawing.Point(805, 12);
            this.btn_Deco.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn_Deco.Name = "btn_Deco";
            this.btn_Deco.Size = new System.Drawing.Size(160, 33);
            this.btn_Deco.TabIndex = 5;
            this.btn_Deco.Text = "Déconnexion";
            this.btn_Deco.UseVisualStyleBackColor = true;
            this.btn_Deco.Click += new System.EventHandler(this.btn_Deco_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lst_Info);
            this.groupBox1.Location = new System.Drawing.Point(41, 126);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Size = new System.Drawing.Size(925, 284);
            this.groupBox1.TabIndex = 11;
            this.groupBox1.TabStop = false;
            // 
            // lst_Info
            // 
            this.lst_Info.FormattingEnabled = true;
            this.lst_Info.ItemHeight = 16;
            this.lst_Info.Items.AddRange(new object[] {
            "1",
            "2",
            "3"});
            this.lst_Info.Location = new System.Drawing.Point(5, 14);
            this.lst_Info.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.lst_Info.Name = "lst_Info";
            this.lst_Info.Size = new System.Drawing.Size(913, 260);
            this.lst_Info.TabIndex = 7;
            this.lst_Info.DoubleClick += new System.EventHandler(this.lst_Info_DoubleClick);
            // 
            // btn_Modifier
            // 
            this.btn_Modifier.Location = new System.Drawing.Point(413, 434);
            this.btn_Modifier.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn_Modifier.Name = "btn_Modifier";
            this.btn_Modifier.Size = new System.Drawing.Size(160, 33);
            this.btn_Modifier.TabIndex = 3;
            this.btn_Modifier.Text = "Modifier";
            this.btn_Modifier.UseVisualStyleBackColor = true;
            this.btn_Modifier.Click += new System.EventHandler(this.btn_Modifier_Click);
            // 
            // Grp_Filtres
            // 
            this.Grp_Filtres.Controls.Add(this.radioButton2);
            this.Grp_Filtres.Controls.Add(this.radioButton1);
            this.Grp_Filtres.Controls.Add(this.Chk_HidePayed);
            this.Grp_Filtres.Location = new System.Drawing.Point(581, 417);
            this.Grp_Filtres.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Grp_Filtres.Name = "Grp_Filtres";
            this.Grp_Filtres.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Grp_Filtres.Size = new System.Drawing.Size(385, 64);
            this.Grp_Filtres.TabIndex = 4;
            this.Grp_Filtres.TabStop = false;
            this.Grp_Filtres.Text = "Filtres";
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(267, 28);
            this.radioButton2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(105, 21);
            this.radioButton2.TabIndex = 2;
            this.radioButton2.Text = "Sort by New";
            this.radioButton2.UseVisualStyleBackColor = true;
            this.radioButton2.CheckedChanged += new System.EventHandler(this.radioButton2_CheckedChanged);
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.radioButton1.Checked = true;
            this.radioButton1.Location = new System.Drawing.Point(139, 28);
            this.radioButton1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(100, 21);
            this.radioButton1.TabIndex = 1;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "Sort by Old";
            this.radioButton1.UseVisualStyleBackColor = true;
            this.radioButton1.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // Chk_HidePayed
            // 
            this.Chk_HidePayed.AutoSize = true;
            this.Chk_HidePayed.Location = new System.Drawing.Point(21, 30);
            this.Chk_HidePayed.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Chk_HidePayed.Name = "Chk_HidePayed";
            this.Chk_HidePayed.Size = new System.Drawing.Size(103, 21);
            this.Chk_HidePayed.TabIndex = 0;
            this.Chk_HidePayed.Text = "Hide Payed";
            this.Chk_HidePayed.UseVisualStyleBackColor = true;
            this.Chk_HidePayed.CheckedChanged += new System.EventHandler(this.Chk_HidePayed_CheckedChanged);
            // 
            // PaymentPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1008, 480);
            this.Controls.Add(this.Grp_Filtres);
            this.Controls.Add(this.btn_Modifier);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btn_Deco);
            this.Controls.Add(this.btn_Supp);
            this.Controls.Add(this.btn_Ajouter);
            this.Controls.Add(this.lbl_DateEcheance);
            this.Controls.Add(this.lbl_DatePaiement);
            this.Controls.Add(this.lbl_Statut);
            this.Controls.Add(this.lbl_Prix);
            this.Controls.Add(this.lbl_Nom);
            this.Controls.Add(this.lbl_ID);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "PaymentPage";
            this.Text = "PaymentPage";
            this.Load += new System.EventHandler(this.PaymentPage_Load);
            this.groupBox1.ResumeLayout(false);
            this.Grp_Filtres.ResumeLayout(false);
            this.Grp_Filtres.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lbl_ID;
        private System.Windows.Forms.Label lbl_Nom;
        private System.Windows.Forms.Label lbl_Prix;
        private System.Windows.Forms.Label lbl_Statut;
        private System.Windows.Forms.Label lbl_DatePaiement;
        private System.Windows.Forms.Label lbl_DateEcheance;
        private System.Windows.Forms.Button btn_Supp;
        private System.Windows.Forms.Button btn_Deco;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btn_Ajouter;
        private System.Windows.Forms.Button btn_Modifier;
        private System.Windows.Forms.ListBox lst_Info;
        private System.Windows.Forms.GroupBox Grp_Filtres;
        private System.Windows.Forms.CheckBox Chk_HidePayed;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton1;
    }
}