﻿/* Author: Alexandre Fontes
 * Date: 05.12.2019
 * Description: View for the LoginPage
 */
using Model;
using System;
using System.Windows.Forms;
using ProjetCSharp_PayZ.View;

namespace ProjetCSharp_PayZ
{
    public partial class Form1 : Form
    {
        Password password = new Password();
        public Form1()
        {
            InitializeComponent();
            txtbox_email.Focus();
        }

        /// <summary>
        /// Function when you click in the button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_valide_Click(object sender, EventArgs e)
        {
            string email = txtbox_email.Text;
            //Try if the login is correct and if it is, the form ValidationPage will open
            try
            {
                if (Login.ValidLogin(email, password.CurrentPassword))
                {
                    PaymentPage paymentPage = new PaymentPage();
                    this.Hide();
                    Session.Email = email;
                    paymentPage.ShowDialog();
                    if (paymentPage.exception == null)
                    {
                        this.Close();
                    }
                    else
                    {
                        paymentPage.Close();
                        this.Show();
                        password = new Password();
                        txtbox_password.Text = "";
                        txtbox_email.Text = "";
                    }
                }
                else 
                {
                    MessageBox.Show("Login incorrect", "ERROR!");
                }
            }
            //Exception for the wrong Password
            catch (WrongPasswordException)
            {
                MessageBox.Show("mdp incorrect");
            }
            //Exception for the wrong Email
            catch (WrongEmailException)
            {
                MessageBox.Show("Login incorrect", "ERROR!");
            }
            //Exception for the SQL connection
            catch (SqlConnectionError)
            {
                MessageBox.Show("Pas de connexion avec le serveur SQL", "ERROR!");
            }

        }

        /// <summary>
        /// Function for the wildcard (Password)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtbox_password_TextChanged(object sender, EventArgs e)
        {
            // More general way to handle inputs for a text event if using multiple text boxes all with the same function
          
                timer1.Enabled = true;
                timer1.Stop();
                timer1.Start();
                TextBox currentInputBox = sender as TextBox;
                password.Change(currentInputBox);
        }

        /// <summary>
        /// Function to return to the RegisterPage
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void brn_return_register_Click(object sender, EventArgs e)
        {
            this.Hide();
            RegisterPage formRegisterPage = new RegisterPage();
            formRegisterPage.ShowDialog();
            this.Show();
            txtbox_email.Text = "";
            txtbox_password.Text = "";
        }

        /// <summary>
        /// Function that changes the text in the text box to * after 1 second
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timer1_Tick(object sender, EventArgs e)
        {
            password.Tick(txtbox_password);
            timer1.Enabled = false;

        }
    }
}
