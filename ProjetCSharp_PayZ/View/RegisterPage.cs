﻿/* Author: Alexandre Fontes
 * Date: 05.12.2019
 * Description: View for the RegisterPage
 */
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Model;

namespace ProjetCSharp_PayZ
{
    public partial class RegisterPage : Form
    {
        Password password = new Password();
        Password passwordConfirm = new Password();

        public RegisterPage()
        {
            InitializeComponent();
            txtbox_email.Focus();
        }

        /// <summary>
        /// Function that check if the email is correct and if the passwords are the same
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_valide_Click(object sender, EventArgs e)
        {
            if(txtbox_email.Text != "" && password.CurrentPassword != "" && passwordConfirm.CurrentPassword != "")
            {
                try
                {
                    //Check if the password is correct
                    if (password.CurrentPassword == passwordConfirm.CurrentPassword)
                    {
                        //The password must have 8 letters
                        if(password.CurrentPassword.Length >= 8)
                        {
                            if (IsValidEmail(txtbox_email.Text))
                            {
                                Register.ValidRegister(txtbox_email.Text, password.CurrentPassword);
                                this.Close();
                            }
                            else
                            {
                                MessageBox.Show("Email non valide", "ERROR!");
                            }
                            
                        }
                        else
                        {
                            MessageBox.Show("Mot de passe trop court (8 caractères minimum)", "ERROR!");
                        }
                    }
                    else
                    {
                        MessageBox.Show("Mot de passe incorrecte", "ERROR!");
                    }
                }
                //Exception if the email exists
                catch (ExistingEmailException)
                {
                    MessageBox.Show("Email déjà existant", "ERROR!");
                }
                //Exception for the SQP connection
                catch (SqlConnectionError)
                {
                    MessageBox.Show("Pas de connexion avec le serveur SQL", "ERROR!");
                }
            }
            else
            {
                MessageBox.Show("Veillez introduire tout les champs", "ERROR!");
            }
        }

        /// <summary>
        /// Function to validate the email address
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        private bool IsValidEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }
        private void txtbox_password_TextChanged(object sender, EventArgs e)
        {
            timer1.Enabled = true;
            timer1.Stop();
            timer1.Start();
            TextBox currentInputBox = sender as TextBox;
            password.Change(currentInputBox);
        }
        private void timer1_Tick(object sender, EventArgs e)
        {
            password.Tick(txtbox_password);
            timer1.Enabled = false;
        }
        private void txtbox_password_confirm_TextChanged(object sender, EventArgs e)
        {
            timer2.Enabled = true;
            timer2.Stop();
            timer2.Start();
            TextBox currentInputBox = sender as TextBox;
            passwordConfirm.Change(currentInputBox);
        }
        private void timer2_Tick(object sender, EventArgs e)
        {
            passwordConfirm.Tick(txtbox_password_confirm);
            timer2.Enabled = false;
        }
    }
}
