﻿/* Auteur: Alexandre Fontes
 * Date: 21.01.2020
 * Description: View for the DetailsPage
 */
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Model;

namespace ProjetCSharp_PayZ.View
{
    public partial class DetailsPage : Form
    {
        Payment payment = new Payment();
        public Exception exception=null;

        /// <summary>
        /// Constructor the load the page
        /// </summary>
        public DetailsPage()
        {
            InitializeComponent();
            payment.IdPayment = -1;
            txtbox_Statut.Text = "0";
        }

        /// <summary>
        /// Constructor that shows the values in the payment
        /// </summary>
        /// <param name="payment"></param>
        public DetailsPage(Payment payment)
        {
            InitializeComponent();
            this.payment = payment;
            ShowPayment();
        }

        /// <summary>
        /// Function that saves the fields
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_enregistrer_Click(object sender, EventArgs e)
        {
            try
            {
                CheckForm();
                payment.PaymentName= txtbox_Nom.Text;
                if (txtbox_NPA.Text != "") payment.PostalCode = int.Parse(txtbox_NPA.Text);
                payment.Address = txtbox_Adresse.Text;
                payment.City = txtbox_Ville.Text;
                payment.RecipientName = txtbox_Beneficiaire.Text;
                payment.RecipientEmail = txtbox_AddrPaiement.Text;
                payment.PayementDate = DateTime.Parse(txtbox_DatePaiement.Text);
                payment.MaxDelay = int.Parse(txtbox_DateEcheance.Text);
                payment.Amount = float.Parse(txtbox_Prix.Text);
                payment.Description = txtbox_Description.Text;
                payment.Status = int.Parse(txtbox_Statut.Text);
                if (payment.IdPayment == -1)
                {
                    PaymentManager.SavePayment(payment);
                }
                else
                {
                    PaymentManager.UpdatePayment(payment);
                }
                this.Close();
            }
            catch (FormatException)
            {

                MessageBox.Show("Veuillez verifier le format des champs");
            }
            catch (ContentException)
            {
                MessageBox.Show("Veuillez verifier que tous les champs sont remplis");
            }
        }

        /// <summary>
        /// Function that shows the payments
        /// </summary>
        private void ShowPayment()
        {
            txtbox_ID.Text = payment.IdPayment + "";
            txtbox_Nom.Text = payment.PaymentName;
            txtbox_NPA.Text = payment.PostalCode +"";
            txtbox_Adresse.Text = payment.Address;
            txtbox_Ville.Text = payment.City;
            txtbox_Beneficiaire.Text = payment.RecipientName;
            txtbox_AddrPaiement.Text = payment.RecipientEmail;
            txtbox_DatePaiement.Text = payment.PayementDate +"";
            txtbox_DateEcheance.Text = payment.MaxDelay +"";
            txtbox_Prix.Text = payment.Amount +"";
            txtbox_Description.Text = payment.Description;
            txtbox_Statut.Text = payment.Status +"";
        }

        /// <summary>
        /// Function to go back
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_retour_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Function to disconnect
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_deco_Click(object sender, EventArgs e)
        {
            exception=new LogoutException();
            this.Close();
        }

        /// <summary>
        /// Function that check if the obligatory fields are checked
        /// </summary>
        private void CheckForm()
        {
            bool cFlag = true;
            bool fFlag = true;
            foreach (Control control in groupBox2.Controls)
            {
                if (control is TextBox)
                {
                    if (control.Text == "")
                    {
                        cFlag = false;
                    }
                }
            }

            int i;
            float f;
            DateTime d;
            if (!int.TryParse(txtbox_Statut.Text, out i)) fFlag = false;
            if (!int.TryParse(txtbox_DateEcheance.Text, out i)) fFlag = false;
            if (!float.TryParse(txtbox_Prix.Text, out f)) fFlag = false;
            if (!DateTime.TryParse(txtbox_DatePaiement.Text, out d)) fFlag = false;
            if (cFlag & !fFlag)
            {
                throw new FormatException();
            }
            if (!cFlag)
            {
                throw new ContentException();
            }
        }
    }

    /// <summary>
    /// Format exception
    /// </summary>
    public class FormatException : Exception
    {

    }

    /// <summary>
    /// Content exception
    /// </summary>
    public class ContentException : Exception
    {

    }
}
