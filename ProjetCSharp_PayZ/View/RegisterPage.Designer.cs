﻿namespace ProjetCSharp_PayZ
{
    partial class RegisterPage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btn_valide = new System.Windows.Forms.Button();
            this.txtbox_password = new System.Windows.Forms.TextBox();
            this.txtbox_email = new System.Windows.Forms.TextBox();
            this.lbl_password = new System.Windows.Forms.Label();
            this.lbl_email = new System.Windows.Forms.Label();
            this.lbl_title = new System.Windows.Forms.Label();
            this.txtbox_password_confirm = new System.Windows.Forms.TextBox();
            this.lbl_password_confirm = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // btn_valide
            // 
            this.btn_valide.Font = new System.Drawing.Font("Rockwell", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_valide.Location = new System.Drawing.Point(165, 243);
            this.btn_valide.Name = "btn_valide";
            this.btn_valide.Size = new System.Drawing.Size(175, 27);
            this.btn_valide.TabIndex = 7;
            this.btn_valide.Text = "Valider votre inscription";
            this.btn_valide.UseVisualStyleBackColor = true;
            this.btn_valide.Click += new System.EventHandler(this.btn_valide_Click);
            // 
            // txtbox_password
            // 
            this.txtbox_password.Location = new System.Drawing.Point(197, 150);
            this.txtbox_password.Name = "txtbox_password";
            this.txtbox_password.Size = new System.Drawing.Size(162, 20);
            this.txtbox_password.TabIndex = 4;
            this.txtbox_password.TextChanged += new System.EventHandler(this.txtbox_password_TextChanged);
            // 
            // txtbox_email
            // 
            this.txtbox_email.Location = new System.Drawing.Point(197, 109);
            this.txtbox_email.Name = "txtbox_email";
            this.txtbox_email.Size = new System.Drawing.Size(162, 20);
            this.txtbox_email.TabIndex = 2;
            // 
            // lbl_password
            // 
            this.lbl_password.AutoSize = true;
            this.lbl_password.Location = new System.Drawing.Point(124, 153);
            this.lbl_password.Name = "lbl_password";
            this.lbl_password.Size = new System.Drawing.Size(71, 13);
            this.lbl_password.TabIndex = 3;
            this.lbl_password.Text = "Mot de passe";
            // 
            // lbl_email
            // 
            this.lbl_email.AutoSize = true;
            this.lbl_email.Location = new System.Drawing.Point(162, 112);
            this.lbl_email.Name = "lbl_email";
            this.lbl_email.Size = new System.Drawing.Size(32, 13);
            this.lbl_email.TabIndex = 1;
            this.lbl_email.Text = "Email";
            // 
            // lbl_title
            // 
            this.lbl_title.AutoSize = true;
            this.lbl_title.Font = new System.Drawing.Font("Rockwell", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_title.Location = new System.Drawing.Point(129, 32);
            this.lbl_title.Name = "lbl_title";
            this.lbl_title.Size = new System.Drawing.Size(190, 39);
            this.lbl_title.TabIndex = 0;
            this.lbl_title.Text = "Inscription";
            // 
            // txtbox_password_confirm
            // 
            this.txtbox_password_confirm.Location = new System.Drawing.Point(197, 186);
            this.txtbox_password_confirm.Name = "txtbox_password_confirm";
            this.txtbox_password_confirm.Size = new System.Drawing.Size(162, 20);
            this.txtbox_password_confirm.TabIndex = 6;
            this.txtbox_password_confirm.TextChanged += new System.EventHandler(this.txtbox_password_confirm_TextChanged);
            // 
            // lbl_password_confirm
            // 
            this.lbl_password_confirm.AutoSize = true;
            this.lbl_password_confirm.Location = new System.Drawing.Point(63, 189);
            this.lbl_password_confirm.Name = "lbl_password_confirm";
            this.lbl_password_confirm.Size = new System.Drawing.Size(131, 13);
            this.lbl_password_confirm.TabIndex = 5;
            this.lbl_password_confirm.Text = "Mot de passe (vérification)";
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // timer2
            // 
            this.timer2.Interval = 1000;
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // RegisterPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(469, 303);
            this.Controls.Add(this.txtbox_password_confirm);
            this.Controls.Add(this.lbl_password_confirm);
            this.Controls.Add(this.btn_valide);
            this.Controls.Add(this.txtbox_password);
            this.Controls.Add(this.txtbox_email);
            this.Controls.Add(this.lbl_password);
            this.Controls.Add(this.lbl_email);
            this.Controls.Add(this.lbl_title);
            this.Name = "RegisterPage";
            this.Text = "RegisterPage";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_valide;
        private System.Windows.Forms.TextBox txtbox_password;
        private System.Windows.Forms.TextBox txtbox_email;
        private System.Windows.Forms.Label lbl_password;
        private System.Windows.Forms.Label lbl_email;
        private System.Windows.Forms.Label lbl_title;
        private System.Windows.Forms.TextBox txtbox_password_confirm;
        private System.Windows.Forms.Label lbl_password_confirm;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Timer timer2;
    }
}