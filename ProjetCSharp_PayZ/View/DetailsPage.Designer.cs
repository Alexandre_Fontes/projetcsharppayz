﻿namespace ProjetCSharp_PayZ.View
{
    partial class DetailsPage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btn_enregistrer = new System.Windows.Forms.Button();
            this.txtbox_Beneficiaire = new System.Windows.Forms.TextBox();
            this.txtbox_NPA = new System.Windows.Forms.TextBox();
            this.lbl_Beneficiaire = new System.Windows.Forms.Label();
            this.txtbox_Ville = new System.Windows.Forms.TextBox();
            this.txtbox_Adresse = new System.Windows.Forms.TextBox();
            this.txtbox_ID = new System.Windows.Forms.TextBox();
            this.lbl_NPA = new System.Windows.Forms.Label();
            this.lbl_Ville = new System.Windows.Forms.Label();
            this.lbl_Adresse = new System.Windows.Forms.Label();
            this.lbl_ID = new System.Windows.Forms.Label();
            this.txtbox_Nom = new System.Windows.Forms.TextBox();
            this.lbl_Nom = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtbox_Statut = new System.Windows.Forms.TextBox();
            this.txtbox_Description = new System.Windows.Forms.TextBox();
            this.txtbox_Prix = new System.Windows.Forms.TextBox();
            this.txtbox_DateEcheance = new System.Windows.Forms.TextBox();
            this.txtbox_DatePaiement = new System.Windows.Forms.TextBox();
            this.txtbox_AddrPaiement = new System.Windows.Forms.TextBox();
            this.lbl_Statut = new System.Windows.Forms.Label();
            this.lbl_Description = new System.Windows.Forms.Label();
            this.lbl_Prix = new System.Windows.Forms.Label();
            this.lbl_DateEcheance = new System.Windows.Forms.Label();
            this.lbl_DatePaiement = new System.Windows.Forms.Label();
            this.lbl_AddrPaiement = new System.Windows.Forms.Label();
            this.btn_deco = new System.Windows.Forms.Button();
            this.btn_retour = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(364, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(267, 32);
            this.label1.TabIndex = 1;
            this.label1.Text = "Détails de paiement";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btn_enregistrer);
            this.groupBox1.Controls.Add(this.txtbox_Beneficiaire);
            this.groupBox1.Controls.Add(this.txtbox_NPA);
            this.groupBox1.Controls.Add(this.lbl_Beneficiaire);
            this.groupBox1.Controls.Add(this.txtbox_Ville);
            this.groupBox1.Controls.Add(this.txtbox_Adresse);
            this.groupBox1.Controls.Add(this.txtbox_ID);
            this.groupBox1.Controls.Add(this.lbl_NPA);
            this.groupBox1.Controls.Add(this.lbl_Ville);
            this.groupBox1.Controls.Add(this.lbl_Adresse);
            this.groupBox1.Controls.Add(this.lbl_ID);
            this.groupBox1.Location = new System.Drawing.Point(32, 335);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Size = new System.Drawing.Size(947, 117);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Informations personnelles (optionel)";
            // 
            // btn_enregistrer
            // 
            this.btn_enregistrer.Location = new System.Drawing.Point(776, 74);
            this.btn_enregistrer.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn_enregistrer.Name = "btn_enregistrer";
            this.btn_enregistrer.Size = new System.Drawing.Size(160, 33);
            this.btn_enregistrer.TabIndex = 5;
            this.btn_enregistrer.Text = "Enregistrer";
            this.btn_enregistrer.UseVisualStyleBackColor = true;
            this.btn_enregistrer.Click += new System.EventHandler(this.btn_enregistrer_Click);
            // 
            // txtbox_Beneficiaire
            // 
            this.txtbox_Beneficiaire.Location = new System.Drawing.Point(720, 37);
            this.txtbox_Beneficiaire.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtbox_Beneficiaire.Name = "txtbox_Beneficiaire";
            this.txtbox_Beneficiaire.Size = new System.Drawing.Size(215, 22);
            this.txtbox_Beneficiaire.TabIndex = 4;
            // 
            // txtbox_NPA
            // 
            this.txtbox_NPA.Location = new System.Drawing.Point(96, 69);
            this.txtbox_NPA.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtbox_NPA.Name = "txtbox_NPA";
            this.txtbox_NPA.Size = new System.Drawing.Size(71, 22);
            this.txtbox_NPA.TabIndex = 1;
            // 
            // lbl_Beneficiaire
            // 
            this.lbl_Beneficiaire.AutoSize = true;
            this.lbl_Beneficiaire.Location = new System.Drawing.Point(572, 41);
            this.lbl_Beneficiaire.Name = "lbl_Beneficiaire";
            this.lbl_Beneficiaire.Size = new System.Drawing.Size(134, 17);
            this.lbl_Beneficiaire.TabIndex = 19;
            this.lbl_Beneficiaire.Text = "Nom du beneficiaire";
            // 
            // txtbox_Ville
            // 
            this.txtbox_Ville.Location = new System.Drawing.Point(340, 71);
            this.txtbox_Ville.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtbox_Ville.Name = "txtbox_Ville";
            this.txtbox_Ville.Size = new System.Drawing.Size(209, 22);
            this.txtbox_Ville.TabIndex = 3;
            // 
            // txtbox_Adresse
            // 
            this.txtbox_Adresse.Location = new System.Drawing.Point(340, 31);
            this.txtbox_Adresse.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtbox_Adresse.Name = "txtbox_Adresse";
            this.txtbox_Adresse.Size = new System.Drawing.Size(209, 22);
            this.txtbox_Adresse.TabIndex = 2;
            // 
            // txtbox_ID
            // 
            this.txtbox_ID.Enabled = false;
            this.txtbox_ID.Location = new System.Drawing.Point(96, 32);
            this.txtbox_ID.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtbox_ID.Name = "txtbox_ID";
            this.txtbox_ID.Size = new System.Drawing.Size(68, 22);
            this.txtbox_ID.TabIndex = 0;
            // 
            // lbl_NPA
            // 
            this.lbl_NPA.AutoSize = true;
            this.lbl_NPA.Location = new System.Drawing.Point(36, 74);
            this.lbl_NPA.Name = "lbl_NPA";
            this.lbl_NPA.Size = new System.Drawing.Size(36, 17);
            this.lbl_NPA.TabIndex = 5;
            this.lbl_NPA.Text = "NPA";
            // 
            // lbl_Ville
            // 
            this.lbl_Ville.AutoSize = true;
            this.lbl_Ville.Location = new System.Drawing.Point(273, 74);
            this.lbl_Ville.Name = "lbl_Ville";
            this.lbl_Ville.Size = new System.Drawing.Size(34, 17);
            this.lbl_Ville.TabIndex = 4;
            this.lbl_Ville.Text = "Ville";
            // 
            // lbl_Adresse
            // 
            this.lbl_Adresse.AutoSize = true;
            this.lbl_Adresse.Location = new System.Drawing.Point(273, 33);
            this.lbl_Adresse.Name = "lbl_Adresse";
            this.lbl_Adresse.Size = new System.Drawing.Size(60, 17);
            this.lbl_Adresse.TabIndex = 3;
            this.lbl_Adresse.Text = "Adresse";
            // 
            // lbl_ID
            // 
            this.lbl_ID.AutoSize = true;
            this.lbl_ID.Location = new System.Drawing.Point(36, 34);
            this.lbl_ID.Name = "lbl_ID";
            this.lbl_ID.Size = new System.Drawing.Size(21, 17);
            this.lbl_ID.TabIndex = 0;
            this.lbl_ID.Text = "ID";
            // 
            // txtbox_Nom
            // 
            this.txtbox_Nom.Location = new System.Drawing.Point(165, 55);
            this.txtbox_Nom.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtbox_Nom.Name = "txtbox_Nom";
            this.txtbox_Nom.Size = new System.Drawing.Size(215, 22);
            this.txtbox_Nom.TabIndex = 1;
            // 
            // lbl_Nom
            // 
            this.lbl_Nom.AutoSize = true;
            this.lbl_Nom.Location = new System.Drawing.Point(17, 59);
            this.lbl_Nom.Name = "lbl_Nom";
            this.lbl_Nom.Size = new System.Drawing.Size(115, 17);
            this.lbl_Nom.TabIndex = 0;
            this.lbl_Nom.Text = "Nom du payment";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.txtbox_Statut);
            this.groupBox2.Controls.Add(this.txtbox_Description);
            this.groupBox2.Controls.Add(this.txtbox_Nom);
            this.groupBox2.Controls.Add(this.txtbox_Prix);
            this.groupBox2.Controls.Add(this.txtbox_DateEcheance);
            this.groupBox2.Controls.Add(this.txtbox_DatePaiement);
            this.groupBox2.Controls.Add(this.txtbox_AddrPaiement);
            this.groupBox2.Controls.Add(this.lbl_Statut);
            this.groupBox2.Controls.Add(this.lbl_Nom);
            this.groupBox2.Controls.Add(this.lbl_Description);
            this.groupBox2.Controls.Add(this.lbl_Prix);
            this.groupBox2.Controls.Add(this.lbl_DateEcheance);
            this.groupBox2.Controls.Add(this.lbl_DatePaiement);
            this.groupBox2.Controls.Add(this.lbl_AddrPaiement);
            this.groupBox2.Location = new System.Drawing.Point(32, 114);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox2.Size = new System.Drawing.Size(947, 201);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Informations de paiement (obligatoire)";
            // 
            // txtbox_Statut
            // 
            this.txtbox_Statut.Enabled = false;
            this.txtbox_Statut.Location = new System.Drawing.Point(531, 92);
            this.txtbox_Statut.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtbox_Statut.Name = "txtbox_Statut";
            this.txtbox_Statut.Size = new System.Drawing.Size(391, 22);
            this.txtbox_Statut.TabIndex = 6;
            // 
            // txtbox_Description
            // 
            this.txtbox_Description.Location = new System.Drawing.Point(531, 55);
            this.txtbox_Description.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtbox_Description.Name = "txtbox_Description";
            this.txtbox_Description.Size = new System.Drawing.Size(391, 22);
            this.txtbox_Description.TabIndex = 5;
            // 
            // txtbox_Prix
            // 
            this.txtbox_Prix.Location = new System.Drawing.Point(165, 166);
            this.txtbox_Prix.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtbox_Prix.Name = "txtbox_Prix";
            this.txtbox_Prix.Size = new System.Drawing.Size(215, 22);
            this.txtbox_Prix.TabIndex = 4;
            // 
            // txtbox_DateEcheance
            // 
            this.txtbox_DateEcheance.Location = new System.Drawing.Point(593, 129);
            this.txtbox_DateEcheance.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtbox_DateEcheance.Name = "txtbox_DateEcheance";
            this.txtbox_DateEcheance.Size = new System.Drawing.Size(55, 22);
            this.txtbox_DateEcheance.TabIndex = 7;
            // 
            // txtbox_DatePaiement
            // 
            this.txtbox_DatePaiement.Location = new System.Drawing.Point(165, 129);
            this.txtbox_DatePaiement.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtbox_DatePaiement.Name = "txtbox_DatePaiement";
            this.txtbox_DatePaiement.Size = new System.Drawing.Size(215, 22);
            this.txtbox_DatePaiement.TabIndex = 3;
            // 
            // txtbox_AddrPaiement
            // 
            this.txtbox_AddrPaiement.Location = new System.Drawing.Point(165, 89);
            this.txtbox_AddrPaiement.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtbox_AddrPaiement.Name = "txtbox_AddrPaiement";
            this.txtbox_AddrPaiement.Size = new System.Drawing.Size(215, 22);
            this.txtbox_AddrPaiement.TabIndex = 2;
            // 
            // lbl_Statut
            // 
            this.lbl_Statut.AutoSize = true;
            this.lbl_Statut.Location = new System.Drawing.Point(445, 95);
            this.lbl_Statut.Name = "lbl_Statut";
            this.lbl_Statut.Size = new System.Drawing.Size(45, 17);
            this.lbl_Statut.TabIndex = 11;
            this.lbl_Statut.Text = "Statut";
            // 
            // lbl_Description
            // 
            this.lbl_Description.AutoSize = true;
            this.lbl_Description.Location = new System.Drawing.Point(445, 55);
            this.lbl_Description.Name = "lbl_Description";
            this.lbl_Description.Size = new System.Drawing.Size(79, 17);
            this.lbl_Description.TabIndex = 10;
            this.lbl_Description.Text = "Description";
            // 
            // lbl_Prix
            // 
            this.lbl_Prix.AutoSize = true;
            this.lbl_Prix.Location = new System.Drawing.Point(17, 166);
            this.lbl_Prix.Name = "lbl_Prix";
            this.lbl_Prix.Size = new System.Drawing.Size(31, 17);
            this.lbl_Prix.TabIndex = 9;
            this.lbl_Prix.Text = "Prix";
            // 
            // lbl_DateEcheance
            // 
            this.lbl_DateEcheance.AutoSize = true;
            this.lbl_DateEcheance.Location = new System.Drawing.Point(445, 129);
            this.lbl_DateEcheance.Name = "lbl_DateEcheance";
            this.lbl_DateEcheance.Size = new System.Drawing.Size(122, 17);
            this.lbl_DateEcheance.TabIndex = 8;
            this.lbl_DateEcheance.Text = "Délai de paiement";
            // 
            // lbl_DatePaiement
            // 
            this.lbl_DatePaiement.AutoSize = true;
            this.lbl_DatePaiement.Location = new System.Drawing.Point(17, 129);
            this.lbl_DatePaiement.Name = "lbl_DatePaiement";
            this.lbl_DatePaiement.Size = new System.Drawing.Size(120, 17);
            this.lbl_DatePaiement.TabIndex = 7;
            this.lbl_DatePaiement.Text = "Date de paiement";
            // 
            // lbl_AddrPaiement
            // 
            this.lbl_AddrPaiement.AutoSize = true;
            this.lbl_AddrPaiement.Location = new System.Drawing.Point(17, 92);
            this.lbl_AddrPaiement.Name = "lbl_AddrPaiement";
            this.lbl_AddrPaiement.Size = new System.Drawing.Size(142, 17);
            this.lbl_AddrPaiement.TabIndex = 6;
            this.lbl_AddrPaiement.Text = "Adresse de paiement";
            // 
            // btn_deco
            // 
            this.btn_deco.Location = new System.Drawing.Point(819, 12);
            this.btn_deco.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn_deco.Name = "btn_deco";
            this.btn_deco.Size = new System.Drawing.Size(160, 33);
            this.btn_deco.TabIndex = 4;
            this.btn_deco.Text = "Déconnexion";
            this.btn_deco.UseVisualStyleBackColor = true;
            this.btn_deco.Click += new System.EventHandler(this.btn_deco_Click);
            // 
            // btn_retour
            // 
            this.btn_retour.Location = new System.Drawing.Point(819, 62);
            this.btn_retour.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn_retour.Name = "btn_retour";
            this.btn_retour.Size = new System.Drawing.Size(160, 33);
            this.btn_retour.TabIndex = 4;
            this.btn_retour.Text = "Retour";
            this.btn_retour.UseVisualStyleBackColor = true;
            this.btn_retour.Click += new System.EventHandler(this.btn_retour_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(445, 146);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 17);
            this.label2.TabIndex = 12;
            this.label2.Text = "(en jours)";
            // 
            // DetailsPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1008, 479);
            this.Controls.Add(this.btn_retour);
            this.Controls.Add(this.btn_deco);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "DetailsPage";
            this.Text = "DetailsPage";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label lbl_Nom;
        private System.Windows.Forms.Label lbl_ID;
        private System.Windows.Forms.Button btn_deco;
        private System.Windows.Forms.Button btn_retour;
        private System.Windows.Forms.Label lbl_NPA;
        private System.Windows.Forms.Label lbl_Ville;
        private System.Windows.Forms.Label lbl_Adresse;
        private System.Windows.Forms.Label lbl_Statut;
        private System.Windows.Forms.Label lbl_Description;
        private System.Windows.Forms.Label lbl_Prix;
        private System.Windows.Forms.Label lbl_DateEcheance;
        private System.Windows.Forms.Label lbl_DatePaiement;
        private System.Windows.Forms.Label lbl_AddrPaiement;
        private System.Windows.Forms.TextBox txtbox_NPA;
        private System.Windows.Forms.TextBox txtbox_Ville;
        private System.Windows.Forms.TextBox txtbox_Adresse;
        private System.Windows.Forms.TextBox txtbox_Nom;
        private System.Windows.Forms.TextBox txtbox_ID;
        private System.Windows.Forms.TextBox txtbox_Statut;
        private System.Windows.Forms.TextBox txtbox_Description;
        private System.Windows.Forms.TextBox txtbox_Prix;
        private System.Windows.Forms.TextBox txtbox_DateEcheance;
        private System.Windows.Forms.TextBox txtbox_DatePaiement;
        private System.Windows.Forms.TextBox txtbox_AddrPaiement;
        private System.Windows.Forms.Button btn_enregistrer;
        private System.Windows.Forms.TextBox txtbox_Beneficiaire;
        private System.Windows.Forms.Label lbl_Beneficiaire;
        private System.Windows.Forms.Label label2;
    }
}