﻿/* Author: Alexandre Fontes
 * Date: 05.12.2019
 * Description: View for the ValidationPage
 */
using Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjetCSharp_PayZ
{
    public partial class ValidationPage : Form
    {
        /// <summary>
        /// Constructor that executes when you open the form
        /// </summary>
        public ValidationPage()
        {
            InitializeComponent();
            lbl_welcome.Text = Session.Email + ", vous êtes bien inscrit-e";
            Session.UpdateSettings();
        }

        /// <summary>
        /// Function that executes itself when the form is loaded
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ValidationPage_Load(object sender, EventArgs e)
        {
            UpdateView();
        }

        /// <summary>
        /// Function that exits the application when you click the button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_connect_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        /// <summary>
        /// Function that updates the view
        /// </summary>
        private void UpdateView()
        {
            this.Location = new Point(Session.Settings.PositionX, Session.Settings.PositionY);
            this.Size = new Size(Session.Settings.SizeX, Session.Settings.SizeY);
        }

        /// <summary>
        /// Function that saves the positions when the form closes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ValidationPage_FormClosed(object sender, FormClosedEventArgs e)
        {
            Session.Settings.PositionX = this.Location.X;
            Session.Settings.PositionY = this.Location.Y;
            Session.Settings.SizeY = this.Size.Height;
            Session.Settings.SizeX = this.Size.Width;
            Session.SaveSettings();
        }
    }
}
