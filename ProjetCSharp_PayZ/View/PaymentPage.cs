﻿/* Author: Alexandre Fontes
 * Date: 21.01.2020
 * Description: View for the paymentPage
 */
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Model;

namespace ProjetCSharp_PayZ.View
{
    public partial class PaymentPage : Form
    {
        List<Payment> payments;
        public Exception exception;
        public PaymentPage()
        {
            InitializeComponent();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (lst_Info.SelectedIndex != -1)
            {
                PaymentManager.DeletePayment(payments[lst_Info.SelectedIndex]);
                showPayments();
            }
        }

        /// <summary>
        /// Function that load the page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PaymentPage_Load(object sender, EventArgs e)
        {
            showPayments();

        }

        /// <summary>
        /// Function that open the DetailsPage
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Ajouter_Click(object sender, EventArgs e)
        {
            DetailsPage Page2 = new DetailsPage();
            this.Hide();
            Page2.ShowDialog();
            if (Page2.exception == null)
            {
                this.Show();
                showPayments();
            }
            else
            {
                exception = Page2.exception;
                this.Close();
            }
        }

        /// <summary>
        /// Function that modify the payments
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Modifier_Click(object sender, EventArgs e)
        {
            if (lst_Info.SelectedIndex != -1)
            {
                DetailsPage Page2 = new DetailsPage(payments[lst_Info.SelectedIndex]);
                this.Hide();
                Page2.ShowDialog();
                if (Page2.exception == null)
                {
                    this.Show();
                    showPayments();
                }
                else
                {
                    exception = Page2.exception;
                    this.Close();
                }
            }
        }

        /// <summary>
        /// Funcion that show the payments
        /// </summary>
        private void showPayments()
        {
            lst_Info.Items.Clear();
            payments = PaymentManager.GetUserPayments(Chk_HidePayed.Checked,radioButton1.Checked);
            foreach (Payment payment in payments)
            {
                lst_Info.Items.Add(payment.IdPayment + " " + payment.PaymentName + " " + payment.Amount + " " + StatusToText(payment) + " " + payment.PayementDate.ToString("dd/MM/yyyy") + " " + payment.PayementDate.AddDays(payment.MaxDelay).ToString("dd/MM/yyyy"));
            }
        }

        /// <summary>
        /// Function to disconnect
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Deco_Click(object sender, EventArgs e)
        {
            exception = new LogoutException();
            this.Close(); 
        }

        /// <summary>
        /// Function to set the status
        /// </summary>
        /// <param name="payment"></param>
        /// <returns></returns>
        private string StatusToText(Payment payment)
        {
            string text = "N/A";
            if (payment.Status == 0)
            {
                if (DateTime.Now > payment.PayementDate.AddDays(payment.MaxDelay))
                {
                    text = "Late";
                }
                else
                {
                    text = "Not Payed";
                }
            }
            else
            {
                text = "Payed";
            }
            return text;
        }

        /// <summary>
        /// Function to sort by old
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton1.Checked)
            {
                radioButton2.Checked = false;
            }
            showPayments();
        }

        /// <summary>
        /// Function to sort by new
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton2.Checked)
            {
                radioButton1.Checked = false;
            }
            showPayments();
        }

        /// <summary>
        /// Function to hide the payments
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Chk_HidePayed_CheckedChanged(object sender, EventArgs e)
        {
            showPayments();
        }

        /// <summary>
        /// Function that pays the selected payment (for testing purpuse only)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lst_Info_DoubleClick(object sender, EventArgs e)
        {
            if (lst_Info.SelectedIndex != -1)
            {
                if (payments[lst_Info.SelectedIndex].Status == 0)
                {
                    DialogResult dialogResult = MessageBox.Show("Voullez-vous confirmer votre paiement de "+ payments[lst_Info.SelectedIndex].Amount.ToString("0.00") + ".- ?", "Confirmation payement", MessageBoxButtons.YesNo);
                    if (dialogResult == DialogResult.Yes)
                    {
                        PaymentManager.Pay(payments[lst_Info.SelectedIndex]);
                        showPayments();
                    }
                }
            }
            
        }
    } 
    public class LogoutException : Exception { }
}
