﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Model
{
    [TestClass]
    public class LoginTest
    {
        /// <summary>
        /// Tests if a normal login works
        /// </summary>
        [TestMethod]
        public void ValidLogin_BaseTest_Success()
        {
            //given
            string email = "maurx18@gmail.com";
            string password = "123456";
            bool actualResult;
            //then
            actualResult = Login.ValidLogin(email, password);
            //when
            Assert.IsTrue(actualResult);

        }
        /// <summary>
        /// Tests if the right exception is thrown when the password is wrong
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(WrongPasswordException))]
        public void ValidLogin_WrongPassword_Exception()
        {
            //given
            string email = "maurx18@gmail.com";
            string password = "1234567";
            bool actualResult;
            //then
            actualResult = Login.ValidLogin(email, password);
            //when
            Assert.IsFalse(actualResult);
        }
        /// <summary>
        /// Tests if the right exception is thrown when the email is wrong
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(WrongEmailException))]
        public void ValidLogin_WrongEmail_Exception()
        {
            //given
            string email = "maurx17@gmail.com";
            string password = "123456";
            //then
            Login.ValidLogin(email, password);
            //when
        }
    }
    [TestClass]
    public class RegisterTest
    {
        /// <summary>
        /// Tests if a register login works
        /// </summary>
        [TestMethod]
        public void ValidRegister_BaseTest_Success()
        {
            //given
            string email = "maurx123@gmail.com";
            string password = "123456";
            bool actualResult;
            //then
            DbConnector connector = new DbConnector();
            connector.Insert("DELETE FROM users WHERE Email='" + email + "';");
            actualResult = Register.ValidRegister(email, password);
            //when
            Assert.IsTrue(actualResult);
        }
        /// <summary>
        /// Tests if the right exception is thrown when the email is already on the database
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ExistingEmailException))]
        public void ValidRegister_ExistingEmail_Exception()
        {
            //given
            string email = "maurx18@gmail.com";
            string password = "123456";
            //then
            DbConnector connector = new DbConnector();
            Register.ValidRegister(email, password);
        }

    }
    [TestClass]
    public class UserSettingsTest
    {
        /// <summary>
        /// Tests if the Json file is read correctly 
        /// </summary>
        [TestMethod]
        public void Json_BaseTest_Success()
        {
            //given
            string email = "maurx123@gmail.com";
            UserSettings expectedResult=new UserSettings();
            UserSettings actualResult;
            expectedResult.PositionX = 0;
            expectedResult.PositionY = 0;
            expectedResult.SizeX = 800;
            expectedResult.SizeY = 600;
            //then
            actualResult =Json.Read(email);
            //when
            Assert.AreEqual(expectedResult.PositionX,actualResult.PositionX);
            Assert.AreEqual(expectedResult.PositionY, actualResult.PositionY);
            Assert.AreEqual(expectedResult.SizeX, actualResult.SizeX);
            Assert.AreEqual(expectedResult.SizeY, actualResult.SizeY);
        }
    }
}