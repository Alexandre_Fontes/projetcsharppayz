CREATE USER 'userpayz'@'%' IDENTIFIED BY 'Pa$$w0rd';
GRANT USAGE ON *.* TO 'userpayz'@'%';
GRANT SELECT, INSERT, UPDATE, DELETE, DROP  ON `payz`.* TO 'userpayz'@'%';
FLUSH PRIVILEGES;